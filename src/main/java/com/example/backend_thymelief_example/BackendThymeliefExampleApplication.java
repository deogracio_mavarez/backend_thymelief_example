package com.example.backend_thymelief_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendThymeliefExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendThymeliefExampleApplication.class, args);
	}

}
