package com.example.backend_thymelief_example.controller;

import com.example.backend_thymelief_example.controller.mapper.ContactMapper;
import com.example.backend_thymelief_example.entity.Contact;
import com.example.backend_thymelief_example.model.ContactsModel;
import com.example.backend_thymelief_example.service.ContactService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/contacts")
@PreAuthorize("hasRole('ROLE_USER')")
public class ContactsController {

    private static final String DEFAULT_VIEW_CONTACS = "contacts";

    private static final String DEFAULT_RESPONSE_METHOD = "result";


    private ContactMapper contactMapper;

    private ContactService contactService;

    @Autowired
    public ContactsController(ContactService contactService) {
        setContactService(contactService);
        setContactMapper(Mappers.getMapper(ContactMapper.class));
    }

    @GetMapping("/cancel")
    public String cancelAdd() {
        return "redirect:/contacts/contactForm?id=0";
    }

    @GetMapping("/contactForm")
    public String getAddForm(@RequestParam(name = "id", required = true) int id, Model model) {
        ContactsModel contactsModel = new ContactsModel();

        if (id != 0){
            contactsModel = contactMapper.toModel(contactService.findById(id));
        }
            model.addAttribute("contactModel", contactsModel);
        return "/contactform";
    }

    @PostMapping("/add")
    public ModelAndView addContact(@ModelAttribute(name = "contactModel") ContactsModel contactsModel, Model model) {
        Contact contact = contactService.addContac(contactMapper.toEntity(contactsModel));
        if (contact != null) {
            model.addAttribute(DEFAULT_RESPONSE_METHOD, 1);
        } else {
            model.addAttribute(DEFAULT_RESPONSE_METHOD, 0);

        }
        return showView();
    }

    @GetMapping("/remove")
    public ModelAndView remove(@RequestParam(name = "id", required = true) int id, Model model) {
        int result = contactService.remove(id);

        if (result == 1) {
            model.addAttribute(DEFAULT_RESPONSE_METHOD, 1);
        } else {
            model.addAttribute(DEFAULT_RESPONSE_METHOD, 0);

        }
        return showView();
    }

    public ModelAndView showView() {
        ModelAndView modelAndView = new ModelAndView(DEFAULT_VIEW_CONTACS);
        List<ContactsModel> contactsModel = contactMapper.toListModel(contactService.listAll());
        modelAndView.addObject(DEFAULT_VIEW_CONTACS, contactsModel);
        return modelAndView;
    }

    public void setContactMapper(ContactMapper contactMapper) {
        this.contactMapper = contactMapper;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }


}
