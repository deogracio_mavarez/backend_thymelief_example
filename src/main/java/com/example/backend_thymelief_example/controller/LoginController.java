package com.example.backend_thymelief_example.controller;

import com.example.backend_thymelief_example.controller.mapper.ContactMapper;
import com.example.backend_thymelief_example.model.ContactsModel;
import com.example.backend_thymelief_example.model.UserCredential;
import com.example.backend_thymelief_example.service.ContactService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
public class LoginController {

    private ContactMapper contactMapper;

    private ContactService contactService;

    private static final String DEFAULT_VIEW_CONTACS = "contacts";

    @Autowired
    public LoginController(ContactService contactService) {
        setContactService(contactService);
        setContactMapper(Mappers.getMapper(ContactMapper.class));
    }

    private static final Log LOG = LogFactory.getLog(LoginController.class);

    @GetMapping("/login")
    public String getLogin(Model model,
                           @RequestParam(name = "error", required = false) String error,
                           @RequestParam(name = "logout", required = false) String logout) {
        model.addAttribute("userCredential", new UserCredential());
        model.addAttribute("logout", logout);
        model.addAttribute("error", error);
        LOG.info("METHOD: getLogin()  ---error: " + error + " logout: " + logout + "");
        return "login";
    }


    @GetMapping("/validateUser")
    public ModelAndView validateCredential() {
        ModelAndView modelAndView = new ModelAndView(DEFAULT_VIEW_CONTACS);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<ContactsModel> contactsModel = contactMapper.toListModel(contactService.listAll());
        modelAndView.addObject("username", user.getUsername());
        modelAndView.addObject(DEFAULT_VIEW_CONTACS, contactsModel);
        return modelAndView;
    }

    public void setContactMapper(ContactMapper contactMapper) {
        this.contactMapper = contactMapper;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }


}
