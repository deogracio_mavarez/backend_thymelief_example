package com.example.backend_thymelief_example.controller.mapper;

import com.example.backend_thymelief_example.entity.Contact;
import com.example.backend_thymelief_example.model.ContactsModel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ContactMapper {

    List<ContactsModel> toListModel(List<Contact> contacts);

    List<Contact> toListEntity(List<ContactsModel> contactsModels);

    ContactsModel toModel(Contact contact);

    Contact toEntity(ContactsModel contactsModel);

}
