package com.example.backend_thymelief_example.entity;


import javax.persistence.*;

@Entity
@Table(name = "users_roles" , uniqueConstraints = @UniqueConstraint(
        columnNames = {"role" , "userName"}
))
public class UserRole {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "username", nullable = false)
    private Users user;

    @Column(name = "role", nullable = false, length = 45)
    private String role;

    public UserRole(){}

    public UserRole(Users user, String role) {
        this.user = user;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
