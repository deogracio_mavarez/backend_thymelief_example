package com.example.backend_thymelief_example.model;

public class ContactsModel {

    private Integer id;
    private String firstName;
    private String lastName;
    private String telePhone;
    private String city;

    public ContactsModel(Integer id, String firstName, String lastName, String telePhone, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telePhone = telePhone;
        this.city = city;
    }

    public ContactsModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelePhone() {
        return telePhone;
    }

    public void setTelePhone(String telePhone) {
        this.telePhone = telePhone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
