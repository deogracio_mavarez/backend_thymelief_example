package com.example.backend_thymelief_example.repository;

import com.example.backend_thymelief_example.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface UsersRepository extends JpaRepository <Users, Serializable>{

    Users findByUserName(String userName);

}
