package com.example.backend_thymelief_example.service.impl;

import com.example.backend_thymelief_example.entity.Contact;
import com.example.backend_thymelief_example.repository.ContactsRepository;
import com.example.backend_thymelief_example.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    public ContactsRepository contactsRepository;

    @Override
    public Contact addContac(Contact contact) {
        return contactsRepository.save(contact);
    }

    @Override
    public List<Contact> listAll() {
        return contactsRepository.findAll();
    }

    @Override
    public int remove(int id) {
        Optional<Contact> contact = Optional.of(contactsRepository.findById(id).orElseThrow(() -> new NullPointerException()));
        contactsRepository.delete(contact.get());
        return 1;
    }

    @Override
    public Contact findById(int id) {
        return contactsRepository.findById(id).get();
    }
}
