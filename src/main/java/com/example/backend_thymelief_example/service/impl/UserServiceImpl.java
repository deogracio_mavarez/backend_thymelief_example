package com.example.backend_thymelief_example.service.impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.example.backend_thymelief_example.entity.UserRole;
import com.example.backend_thymelief_example.entity.Users;
import com.example.backend_thymelief_example.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userService")
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Users user = usersRepository.findByUserName(userName);
    List<GrantedAuthority> grantedAuthorities = buildAuthority(user.getUserRole());
        BCryptPasswordEncoder p = new BCryptPasswordEncoder();
        System.out.println("user");
        System.out.println(p.encode("user"));
        return buildUser(user,grantedAuthorities);
    }

    private User buildUser(Users users, List<GrantedAuthority> grantedAuthority) {
        return new User(users.getUserName(),users.getPassword(),users.isEnabled(),
                true,true,true,grantedAuthority);
    }

    private List<GrantedAuthority> buildAuthority(Set<UserRole> userRoles){
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        for(UserRole userRole : userRoles){
            grantedAuthorities.add(new SimpleGrantedAuthority(userRole.getRole()));
        }
        return new ArrayList<>(grantedAuthorities);

    }
}
