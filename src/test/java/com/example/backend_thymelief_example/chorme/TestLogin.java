package com.example.backend_thymelief_example.chorme;


import com.google.common.annotations.VisibleForTesting;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestLogin {

    private WebDriver driver;

    @org.junit.Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chrome/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://localhost:8080/login");

    }

    @Test
    public void testGoglePage() {

        WebElement searchbox = driver.findElement(By.name("userName"));

        searchbox.clear();

        searchbox.sendKeys("user");

        WebElement searchbox1 = driver.findElement(By.name("password"));

        searchbox1.clear();

        searchbox1.sendKeys("user");

        searchbox.submit();

        assertEquals("http://localhost:8080/validateUser", driver.getCurrentUrl());

    }

    @After
    public void winClose() {

        driver.quit();
    }




}